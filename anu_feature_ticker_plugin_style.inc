<?php
/**
 * ANU feature rotator style.
 */
class anu_feature_ticker_plugin_style extends anu_feature_plugin_style {
  /**
   * Default options.
   */
  function option_definition() {
    $options = views_plugin_style::option_definition();
    $options['title'] = array('default' => '');
    $options['link_listing'] = array('default' => '');
    return $options;
  }

  /**
   * Add widgets for selecting fields for the feature rotator.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['help_ticker'] = array(
      '#prefix' => '<p>',
      '#markup' => t('Note that the title field chosen below should include the title link where appropriate.'),
      '#suffix' => '</p>',
      '#weight' => -5,
    );

    unset($form['image']);
    unset($form['url']);

    $form['link_listing'] = array(
      '#type' => 'textfield',
      '#title' => t('Link to listing'),
      '#default_value' => $this->options['link_listing'],
      '#description' => t('Specify a path or URL to the page with a complete listing.'),
    );
  }
}