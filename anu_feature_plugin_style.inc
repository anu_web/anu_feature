<?php
/**
 * ANU feature rotator style.
 */
class anu_feature_plugin_style extends views_plugin_style {
  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['title'] = array('default' => '');
    $options['image'] = array('default' => '');
    $options['url'] = array('default' => '');
    return $options;
  }

  /**
   * Add widgets for selecting fields for the feature rotator.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $field_labels = $this->display->handler->get_field_labels();
    $field_options = array_map('html_entity_decode', $field_labels);

    $form['help'] = array(
      '#prefix' => '<p>',
      '#markup' => t('Configure fields below to display in their respective places on the feature rotator.'),
      '#suffix' => '</p>',
      '#weight' => -5,
    );

    $form['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#options' => $field_options,
      '#default_value' => $this->options['title'],
      '#required' => TRUE,
      '#description' => t('Select a field containing the featured item title.'),
    );
    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image'),
      '#options' => $field_options,
      '#default_value' => $this->options['image'],
      '#required' => TRUE,
      '#description' => t('Select a field for displaying an image.'),
      '#weight' => 1,
    );
    $form['url'] = array(
      '#type' => 'select',
      '#title' => t('More link URL'),
      '#options' => $field_options,
      '#default_value' => $this->options['url'],
      '#description' => t('URL for the "read more" link at the bottom of the featured item text. If this option is omitted, "read more" links will have to be manually inserted.'),
      '#weight' => 2,
    );

    $text_help = '';
    if ($this->row_plugin->uses_fields()) {
      $text_help .= '<p>';
      $text_help .= t('All fields not excluded from display will be part of the feature item excerpt. The following fields will be included:');
      $text_help .= '</p>';
      $fields = array();
      $field_options = $this->display->handler->get_option('fields');
      foreach ($field_labels as $name => $label) {
        if (empty($field_options[$name]['exclude'])) {
          $fields[] = $label;
        }
      }
      if (!empty($fields)) {
        $text_help .= '<ul>';
        foreach ($fields as $field) {
          $text_help .= '<li>' . $field . '</li>';
        }
        $text_help .= '</ul>';
      }
    }
    elseif ($row_plugin_title = $this->row_plugin->plugin_title()) {
      $text_help .= '<p>';
      $text_help .= t('The %row_plugin_title in each row will be the feature item excerpt.', array('%row_plugin_title' => $row_plugin_title));
      $text_help .= '</p>';
    }

    $form['text_help'] = array(
      '#type' => 'item',
      '#title' => t('Excerpt'),
      '#markup' => $text_help,
      '#weight' => 5,
    );
  }
}