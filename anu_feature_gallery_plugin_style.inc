<?php
/**
 * ANU large gallery feature rotator style.
 */
class anu_feature_gallery_plugin_style extends anu_feature_plugin_style {
  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['image_small'] = array('default' => '');
    $options['image_thumbnail'] = array('default' => '');
    return $options;
  }

  /**
   * Add widgets for selecting fields for the feature rotator.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $field_labels = $this->display->handler->get_field_labels();
    foreach ($field_labels as $field => $label) {
      $field_labels[$field] = html_entity_decode($label);
    }

    $form['image_small'] = $form['image'];
    $form['image_thumbnail'] = $form['image'];

    $form['image']['#description'] = t('Select a field for displaying an image. Its dimensions should be 640x360.');
    $form['image_small']['#title'] = t('Image - small');
    $form['image_small']['#default_value'] = $this->options['image_small'];
    $form['image_small']['#description'] = t('Select a field for displaying a smaller version of the image. Its dimensions should be 280x360.');
    $form['image_small']['#weight'] = 1.1;
    $form['image_thumbnail']['#title'] = t('Image - thumbnail');
    $form['image_thumbnail']['#default_value'] = $this->options['image_thumbnail'];
    $form['image_thumbnail']['#description'] = t('Select a field for displaying an image thumbnail. Its dimensions should be 90x90');
    $form['image_thumbnail']['#weight'] = 1.2;
  }
}