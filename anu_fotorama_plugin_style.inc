<?php
/**
 * ANU feature rotator style.
 */
class anu_fotorama_plugin_style extends views_plugin_style {
  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['ratio'] = array('default' => '16/9');
    $options['transition'] = array('default' => 'slide');
    $options['nav'] = array('default' => FALSE);
    $options['arrows'] = array('default' => TRUE);
    $options['click'] = array('default' => FALSE);
    $options['swipe'] = array('default' => TRUE);
    $options['trackpad'] = array('default' => TRUE);
    $options['allowfullscreen'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Add widgets for selecting fields for the feature rotator.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['ratio'] = array(
      '#type' => 'textfield',
      '#title' => t('Ratio'),
      '#default_value' => $this->options['ratio'],
      '#description' => t('Width divided by height. If left blank, the gallery will attempt to resize itself on transition.'),
    );
    $form['glimpse'] = array(
      '#type' => 'textfield',
      '#title' => t('Glimpse'),
      '#default_value' => $this->options['glimpse'],
      '#description' => t('Glimpse size of nearby frames in pixels or percents.'),
    );
    $form['startindex'] = array(
      '#type' => 'textfield',
      '#title' => t('Startindex'),
      '#default_value' => $this->options['startindex'],
      '#description' => t('Index or id of the frame that will be shown upon initialization of the fotorama.'),
    );
    $form['margin'] = array(
      '#type' => 'textfield',
      '#title' => t('Margin'),
      '#default_value' => $this->options['margin'],
      '#description' => t('Horizontal margins for frames in pixels.'),
    );
    $form['transition'] = array(
      '#type' => 'select',
      '#title' => t('Transition'),
      '#options' => array(
        'slide' => t('Slide'),
        'crossfade' => t('Crossfade'),
      ),
      '#default_value' => $this->options['transition'],
    );
    $form['nav'] = array(
      '#type' => 'select',
      '#title' => t('Navigation style'),
      '#options' => array(
        FALSE => t('Nothing'),
        'dots' => t('iPhone-style dots'),
        'thumbs' => t('Thumbnails'),
      ),
      '#default_value' => $this->options['nav'],
    );
    $form['arrows'] = array(
      '#type' => 'select',
      '#title' => t('Arrows'),
      '#options' => array(
        TRUE => t('True'),
        FALSE => t('False'),
        'always' => t('Always (do not hide controls on hover or tap)'),
      ),
      '#default_value' => $this->options['arrows'],
      '#description' => t('Turns on navigation arrows over the frames'),
    );
    $form['click'] = array(
      '#type' => 'checkbox',
      '#title' => t('Click'),
      '#default_value' => $this->options['click'],
      '#description' => t('Moving between frames by clicking'),
    );
    $form['swipe'] = array(
      '#type' => 'checkbox',
      '#title' => t('Swipe'),
      '#default_value' => $this->options['swipe'],
      '#description' => t('Moving between frames by swiping'),
    );
    $form['trackpad'] = array(
      '#type' => 'checkbox',
      '#title' => t('Trackpad'),
      '#default_value' => $this->options['trackpad'],
      '#description' => t('Enables trackpad support and horizontal mouse wheel as well'),
    );
    $form['allowfullscreen'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow fullscreen'),
      '#default_value' => $this->options['allowfullscreen'],
    );
  }
}
