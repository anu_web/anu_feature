<?php
/**
 * @file
 * Views implementations.
 */

/**
 * Implements hook_views_plugins().
 */
function anu_feature_views_plugins() {
  $path = drupal_get_path('module', 'anu_feature');
  $plugins = array(
    'style' => array(
      'anu_feature' => array(
        'title'           => t('ANU feature rotator (ANU window)'),
        'path'            => $path,
        'help'            => ('Display a view of feature items as a homepage feature.'),
        'handler'         => 'anu_feature_plugin_style',
        'theme'           => 'views_view_anu_feature',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_feature_standard' => array(
        'title'           => t('ANU feature rotator (no window)'),
        'path'            => $path,
        'help'            => ('Display a view of feature items.'),
        'handler'         => 'anu_feature_plugin_style',
        'theme'           => 'views_view_anu_feature',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_feature_wide' => array(
        'title'           => t('ANU feature rotator (wide)'),
        'path'            => $path,
        'help'            => ('Display a view of feature items.'),
        'handler'         => 'anu_feature_plugin_style',
        'theme'           => 'views_view_anu_feature',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_feature_narrow' => array(
        'title'           => t('ANU feature rotator (narrow)'),
        'path'            => $path,
        'help'            => ('Display a view of feature items.'),
        'handler'         => 'anu_feature_plugin_style',
        'theme'           => 'views_view_anu_feature',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_feature_ticker' => array(
        'title'           => t('ANU feature rotator (ticker)'),
        'path'            => $path,
        'help'            => ('Display a featured notices ticker.'),
        'handler'         => 'anu_feature_ticker_plugin_style',
        'theme'           => 'views_view_anu_feature_ticker',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_feature_gallery' => array(
        'title'           => t('ANU feature rotator (large gallery)'),
        'path'            => $path,
        'help'            => ('Display a view of feature items.'),
        'handler'         => 'anu_feature_gallery_plugin_style',
        'theme'           => 'views_view_anu_feature_gallery',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'help_topic'      => 'style-anu-feature',
      ),
      'anu_fotorama' => array(
        'title'           => t('ANU Fotorama'),
        'help'            => ('Display a Fotorama slideshow.'),
        'handler'         => 'anu_fotorama_plugin_style',
        'theme'           => 'anu_fotorama_view',
        'theme file'      => 'anu_feature.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
      ),
    ),
  );

  return $plugins;
}