(function ($) {

/**
 * Sets up feature rotators.
 */
Drupal.behaviors.anu_feature = {
  attach: function (context, settings) {
    if (settings['anu_feature']) {
      for (var name in settings['anu_feature']) {
        var feature_rotator = $('#anu-features-' + name, context).get(0);
        if (feature_rotator) {
          var feature_settings = settings['anu_feature'][name];
          Drupal.anu_feature.process_feature_rotator(feature_rotator, name);
          Drupal.anu_feature.process_nav_links(feature_rotator, name);
          Drupal.anu_feature.process_nav_pager(feature_rotator, name, feature_settings['nav_pager']);
        }
      }
    }
  }
};

Drupal.anu_feature = Drupal.anu_feature || {};

/**
 * Processes the feature rotator element.
 *
 * @param context
 *   Feature rotator element.
 * @param feature_name
 *   Name of the feature rotator, e.g. 'rotating'.
 */
Drupal.anu_feature.process_feature_rotator = function (context, feature_name) {
  var mouseover = function () {
    pauseFeatureTimer(feature_name);
  };
  var mouseout = function() {
    playFeatureTimer(feature_name);
  };
  $(context).hover(mouseover, mouseout);
};

  /**
 * Processes image navigation button links.
 *
 * @param context
 *   Feature rotator element.
 * @param feature_name
 *   Name of the feature rotator, e.g. 'rotating'.
 */
Drupal.anu_feature.process_nav_links = function (context, feature_name) {
  $('.anu-feature-nav-link-prev', context).click(function () {
    showPrevFeature(feature_name);
    return false;
  });
  $('.anu-feature-nav-link-playpause', context).click(function () {
    pausePlayFeatures(feature_name);
    return false;
  });
  $('.anu-feature-nav-link-next', context).click(function () {
    showNextFeature(feature_name);
    return false;
  });
};

/**
 * Processes feature navigation pager links.
 *
 * @param context
 *   Feature rotator element.
 * @param feature_name
 *   Name of the feature rotator, e.g. 'rotating'.
 * @param map
 *   Associative array keyed by link ID. Each value is an array containing:
 *   - id: Numeric identifier (index) for the associated feature item.
 */
Drupal.anu_feature.process_nav_pager = function (context, feature_name, map) {
  for (var link_id in map) {
    Drupal.anu_feature.process_nav_pager_link(context, feature_name, link_id, map[link_id]['id']);
  }
};

/**
 * Processes an individual feature navigation pager link.
 */
Drupal.anu_feature.process_nav_pager_link = function (context, feature_name, link_id, feature_id) {
  $('#' + link_id, context).click(function () {
    showFeature(feature_id, feature_name);
    return false;
  });
};

/**
 * Prepares tickers.
 */
Drupal.behaviors.anu_feature_ticker = {
  attach: function (context) {
    $('#play-btn', context).hide();
    $('.newsticker', context).newsTicker({
      row_height: 24,
      max_rows: 2,
      duration: 5000,
      speed: 800,
      prevButton: $('#prev-btn', context),
      nextButton: $('#next-btn', context),
      stopButton: $('#pause-btn', context),
      startButton: $('#play-btn', context)
    });
    $('#pause-btn', context).click(function () {
      $('#pause-btn', context).hide();
      $('#play-btn', context).show();
    });
    $('#play-btn', context).click(function () {
      $('#play-btn', context).hide();
      $('#pause-btn', context).show();
    });
  }
};

/**
 * Prepares large gallery rotators.
 */
Drupal.behaviors.anu_feature_gallery = {
  attach: function (context) {
    $('#f4', context).once('anu-feature-gallery', function () {
      var gallery = new Drupal.ANUFeatureGallery(this);
      gallery.play();
    });
  }
};

/**
 * Large gallery feature rotator.
 */
Drupal.ANUFeatureGallery = function (context) {
  this.context = context;
  var self = this;

  // Set default properties.
  this.playing = false;
  this.duration = 5000;
  this.pollInterval = 100;
  this.maxFeatures = 4;
  this.touchEnabled = false;
  try {
    this.touchEnabled = Modernizr && Modernizr.touch;
  }
  catch (e) {}

    // Prepare rotator.
  if (!this.touchEnabled) {
    $(context)
      .mouseenter(function () {
        if (self.isPlaying()) {
          self.pause();
        }
      })
      .mouseleave(function () {
        if (self.isPlaying()) {
          // Resume playing from paused state.
          self.play();
        }
      });
  }

  // Prepare features.
  var counter = 0;
  this.features = [];
  $('#f4-features > div', context).each(function () {
    if (counter < self.maxFeatures) {
      self.features[counter] = this;
      counter++;
    }
  });

  // Prepare feature shorts.
  counter = 0;
  this.shorts = [];
  $('#f4-shorts > div', context).each(function () {
    if (counter < self.maxFeatures) {
      self.shorts[counter] = this;
      var featureShort = $(this);

      // Activate feature on hover for non-touch devices and click.
      if (!self.touchEnabled) {
        featureShort.mouseenter(self._getCallbackActivate(counter));
      }
      // Navigate to referenced content when clicking active link or short.
      var featureLink = $('a', this);
      featureLink.click(self._getCallbackLinkNavigate(counter, featureLink));
      featureShort.css('cursor', 'pointer').click(function () {
        featureLink.trigger('click', [true]);
      });

      counter++;
    }
  });

  // Set up control.
  this.control = $('a.control', context).click(function (event) {
    self.togglePlaying();
    return false;
  });

  // Activate first feature.
  this.setActiveFeature(0);
};

/**
 * Returns callback to trigger slide activation.
 */
Drupal.ANUFeatureGallery.prototype._getCallbackActivate = function (featureIndex) {
  var self = this;
  return function () {
    self.setActiveFeature(featureIndex);
  };
};

/**
 * Returns callback to navigate to active link.
 */
Drupal.ANUFeatureGallery.prototype._getCallbackLinkNavigate = function (featureIndex, context) {
  var self = this;
  var href = $(context)[0].href;
  return function (event, isProxy) {
    // Navigate if active.
    if (self.current == featureIndex) {
      // Directly navigate if proxy event.
      if (isProxy) {
        window.location.href = href;
      }
      // Use default navigation if link event.
      else {
        event.stopPropagation();
        return true;
      }
    }
    // Activate otherwise.
    else {
      self.setActiveFeature(featureIndex);
    }
    return false;
  };
};

/**
 * Toggles the playing state of the gallery.
 */
Drupal.ANUFeatureGallery.prototype.togglePlaying = function () {
  var playing = !this.playing;
  this._setPlaying(playing);
  if (playing) {
    this.play();
  }
  else {
    this.stop();
  }
};

/**
 * Returns whether the rotator is playing.
 */
Drupal.ANUFeatureGallery.prototype.isPlaying = function () {
  return this.playing;
};

/**
 * Sets the playing state of the rotator.
 */
Drupal.ANUFeatureGallery.prototype._setPlaying = function (playing) {
  if (this.playing != playing) {
    this.playing = playing;
    // Update control.
    this.control
      .removeClass('control-state-playing control-state-paused')
      .addClass(playing ? 'control-state-playing' : 'control-state-paused');
  }
};

/**
 * Continuously plays the rotator.
 */
Drupal.ANUFeatureGallery.prototype.play = function () {
  this.paused = false;
  this._setPlaying(true);
  this._poll();
};

/**
 * Continuously polls the rotator to advance on reaching duration.
 */
Drupal.ANUFeatureGallery.prototype._poll = function () {
  if (this.playing && !this.paused) {
    // Prepare timer.
    this.stateTimer = this.stateTimer || new Date().getTime();
    this.stateElapsed = this.stateElapsed || 0;
    // Poll.
    var self = this;
    this.stateTimeout = setTimeout(function () {
      if (self.playing && !self.paused && self.stateTimer) {
        var elapsed = self.stateElapsed + (new Date().getTime() - self.stateTimer);
        if (self.playing && elapsed >= self.duration) {
          self.advance();
        }
        self._poll();
      }
    }, this.pollInterval);
  }
};

/**
 * Pauses the rotator.
 */
Drupal.ANUFeatureGallery.prototype.pause = function () {
  if (!this.paused) {
    this.paused = true;
    // Advance timer.
    var t = new Date().getTime();
    var nowElapsed = this.stateTimer ? (t - this.stateTimer) : 0;
    this.stateElapsed = (this.stateElapsed || 0) + nowElapsed;
    this.stateTimer = 0;
  }
};

/**
 * Stops the rotator.
 */
Drupal.ANUFeatureGallery.prototype.stop = function () {
  if (this.stateTimeout) {
    clearTimeout(this.stateTimeout);
  }
  // Reset state timer.
  this.stateElapsed = 0;
  this.stateTimer = 0;
};

/**
 * Advances the rotator to the next slide.
 */
Drupal.ANUFeatureGallery.prototype.advance = function () {
  // Go to the first one by default.
  var nextIndex = 0;

  // Look for an active slide.
  if (this.current !== undefined) {
    nextIndex = (this.current + 1) % this.maxFeatures;
  }

  // Set active.
  this.setActiveFeature(nextIndex);
};

/**
 * Sets the active feature.
 */
Drupal.ANUFeatureGallery.prototype.setActiveFeature = function (index) {
  if (this.current != index) {
    this.current = index;

    // Activate feature.
    if (this.features[index]) {
      $(this.features).hide();
      $(this.features[index]).show();
    }
    // Highlight active feature short.
    if (this.shorts[index]) {
      $('.f4-short-selected', this.context).removeClass('f4-short-selected');
      $(this.shorts[index]).addClass('f4-short-selected');
    }

    // Reset timer.
    if (this.isPlaying()) {
      this.stateElapsed = 0;
      this.stateTimer = 0;
    }
  }
};

})(jQuery);