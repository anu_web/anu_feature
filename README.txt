ANU Rotating Feature

You can use Views to construct a feature rotator. To use ANU styles in a view,
configure it to use "ANU feature rotator" as the display style.

The default ANU feature rotator style contains three options to configure: the
image, title and "more" link fields. Select each corresponding field from the
view for each option to use it as that component of the rotator. Specifically:

 - Title: text displayed as the <h2> title of the item.
 - Image: a 320x215 image displayed behind the ANU Window image.
 - More link URL: URL text for linking the "read more" text.

The remaining fields will be displayed as normal, omitting any excluded field,
for body text of the featured item.

There are a total of five feature rotator styles:

 - ANU feature rotator (ANU window)
 - ANU feature rotator (no window)
 - ANU feature rotator (wide)
 - ANU feature rotator (narrow)
 - ANU feature rotator (ticker)

These styles have a more or less similar set of options. For more information
on how they are used in terms of ANU styles, please visit:

  http://styles.anu.edu.au/guide/features.php
