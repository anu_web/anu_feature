<?php
/**
 * @file
 * Theme callbacks.
 */

/**
 * Declares theme hooks.
 */
function _anu_feature_theme() {
  $path = drupal_get_path('module', 'anu_feature') . '/theme';
  $base = array(
    'file' => 'anu_feature.theme.inc',
    'path' => $path,
  );

  // Declare generic rotator wrapper.
  $hooks['anu_feature_rotator'] = array(
    'variables' => array(
      'feature_name' => '',
      'style_version' => ACTON_PROFILE_STYLE_VERSION,
      'items' => array(),
      'window' => FALSE,
      'image_position' => 'before',
      'fade' => FALSE,
      'nav_type' => 'minimal',
      'live_preview' => FALSE,
    ),
    'template' => 'anu-feature-rotator',
  ) + $base;

  // Declare element for individual featured item.
  $hooks['anu_feature_box'] = array(
    'variables' => array(
      'id' => '',
      'site' => array(),
      'style_version' => ACTON_PROFILE_STYLE_VERSION,
      'counter' => NULL,
      'window' => FALSE,
      'title' => '',
      'title_path' => '',
      'image' => '',
      'image_position' => 'before',
      'text' => '',
      'url' => '',
    ),
    'template' => 'anu-feature-box',
  ) + $base;

  $hooks['anu_feature_rotator_wide_narrow'] = array(
    'variables' => array('items' => array(), 'live_preview' => FALSE),
    'template' => 'anu-feature-rotator-wide-narrow',
  ) + $base;

  // Define ticker.
  $hooks['anu_feature_ticker'] = array(
    'variables' => array(
      'items' => array(),
      'style_base' => '',
      'link_listing' => '',
      'live_preview' => FALSE,
    ),
    'template' => 'anu-feature-ticker',
  ) + $base;

  // Define gallery rotator.
  $hooks['anu_feature_gallery'] = array(
    'variables' => array(
      'items' => array(),
      'style_base' => '',
      'live_preview' => FALSE,
    ),
    'template' => 'anu-feature-gallery',
  ) + $base;

  return $hooks;
}

/**
 * Implements template_preprocess_HOOK() for views_view_anu_feature.
 */
function template_preprocess_views_view_anu_feature(&$variables) {
  $view = $variables['view'];
  $style_plugin = $view->style_plugin;
  $style_plugin_name = $view->display_handler->get_option('style_plugin');

  // Prepare feature rotator variables.
  $features = anu_feature_theme_feature_names();
  if (isset($features[$style_plugin_name])) {
    $variables['feature_variables'] = anu_feature_theme_feature_options($features[$style_plugin_name]);
    $variables['feature_variables']['live_preview'] = !empty($view->override_path);
  }

  // Prepare items.
  $variables['options'] += array('title' => NULL, 'image' => NULL, 'url' => NULL);
  $variables['feature_variables']['items'] = array();
  foreach ($variables['rows'] as $i => $text) {
    foreach (array('title', 'image', 'url') as $field) {
      $variables['feature_variables']['items'][$i][$field] = $style_plugin->get_field($i, $variables['options'][$field]);
    }
    $variables['feature_variables']['items'][$i]['text'] = $text;
  }
}

/**
 * Implements template_process_HOOK() for views_view_anu_feature.
 */
function template_process_views_view_anu_feature(&$variables) {
  $variables['feature'] = '';

  if (isset($variables['feature_variables'])) {
    $variables['feature'] = theme('anu_feature_rotator', $variables['feature_variables']);
  }
}

/**
 * Returns a map of style plugin names to feature names.
 */
function anu_feature_theme_feature_names() {
  return array(
    'anu_feature' => 'rotating',
    'anu_feature_standard' => 'rotating-2',
    'anu_feature_wide' => 'vertical-w',
    'anu_feature_narrow' => 'vertical',
  );
}

/**
 * Gets anu_feature_rotator variables tailored for a particular named feature.
 */
function anu_feature_theme_feature_options($name) {
  $variables = array('feature_name' => $name);
  switch ($name) {
    case 'rotating':
      $variables['window'] = TRUE;
      $variables['nav_type'] = 'full';
      break;
    case 'rotating-2':
      $variables['image_position'] = 'after';
      $variables['nav_type'] = 'full';
      break;
  }
  return $variables;
}

/**
 * Implements template_preprocess_HOOK() for anu_feature_rotator.
 */
function template_preprocess_anu_feature_rotator(&$variables) {
  // Prepare variables.
  $variables['features'] = array();
  $variables['nav_attributes_array'] = array();
  $variables['nav_link_attributes_array'] = array();

  // Prepare attributes.
  $feature_name = $variables['feature_name'];
  $variables['classes_array'][] = 'noprint';
  $variables['classes_array'][] = 'anu-feature-rotator';
  $variables['attributes_array']['id'] = 'anu-features-' . $feature_name;
  $variables['nav_attributes_array']['id'] = 'features-nav-' . $feature_name;

  global $is_https;
  if ($profile = acton_profile_get_active_profile()) {
    // Build site array.
    $variables['profile'] = $profile;
    $variables['style_base'] = $style_base = ($is_https ? 'https' : 'http') . '://' . $profile->getServerEndpointBase();
    $variables['style_version'] = $style_version = $profile->getStyleVersion();
    $site = compact('profile', 'return_ssl', 'style_base', 'style_version');

    // Render items.
    $counter = 1;
    $settings = array();
    foreach ($variables['items'] as $item) {
      // Add item.
      $item += compact('site', 'counter') + array(
        'id' => 'feature-' . $counter,
        'window' => $variables['window'],
        'image_position' => $variables['image_position'],
      );
      if ($style_version > 3 && !preg_match('/<a\s+/i', $item['title'])) {
        $item['title_path'] = $item['url'];
      }
      $variables['features'][$counter] = theme('anu_feature_box', $item);
      $feature_link_id = 'feature-link-' . $counter;
      $variables['nav_link_attributes_array'][$counter] = array(
        'href' => '#',
        'id' => $feature_link_id,
      );

      // Add Javascript settings for setting up.
      $settings[$feature_name]['nav_pager'][$feature_link_id] = array(
        'id' => $counter,
      );

      // Increase counter for next item.
      $counter ++;
    }
    // Select first.
    if (isset($variables['nav_link_attributes_array'][1])) {
      $variables['nav_link_attributes_array'][1]['class'] = 'linkselect';
    }

    // Add Javascript.
    if ($variables['live_preview']) {
      drupal_set_message(t('The rotator navigation mechanism is disabled in live preview.'), 'warning');
    }
    elseif (!empty($variables['features']) && $variables['style_base']) {
      // Add settings.
      drupal_add_js(array('anu_feature' => $settings), 'setting');
      // Add rotator script when not in live preview.
      static $anu_feature_js = FALSE;
      if (!$anu_feature_js) {
        $anu_feature_js = TRUE;
        $path = drupal_get_path('module', 'anu_feature');
        drupal_add_js($path . '/anu_feature.js');
        drupal_add_js($style_base . '/' . $style_version . '/scripts/anu-feature-jq.js', array(
          'type' => 'external',
          'weight' => 5,
        ));
      }
    }
  }
}

/**
 * Implements template_process_HOOK() for anu_feature_rotator.
 */
function template_process_anu_feature_rotator(&$variables) {
  $variables['nav_attributes'] = $variables['nav_attributes_array'] ? drupal_attributes($variables['nav_attributes_array']) : '';
  $variables['nav_link_attributes'] = array();
  foreach ($variables['nav_link_attributes_array'] as $i => &$nav_link_attributes) {
    $variables['nav_link_attributes'][$i] = $nav_link_attributes ? drupal_attributes($nav_link_attributes) : '';
  }
}

/**
 * Implements template_preprocess_HOOK() for anu_feature_box.
 */
function template_preprocess_anu_feature_box(&$variables) {
  // Prepare attributes.
  $variables['classes_array'][] = 'feature-box';
  $variables['attributes_array']['id'] = 'feature-' . $variables['id'];

  // Clean title.
  $variables['title_clean'] = strip_tags($variables['title']);
}

/**
 * Implements template_process_HOOK() for anu_feature_box.
 */
function template_process_anu_feature_box(&$variables) {
  if (!empty($variables['title_path'])) {
    $options = array('html' => TRUE);
    if ($variables['title_path'][0] == '/') {
      $options['external'] = TRUE;
    }
    $variables['title'] = l($variables['title'], $variables['title_path'], $options);
  }
}

/**
 * Preprocesses ticker variables.
 */
function theme_views_view_anu_feature_ticker($variables) {
  $view = $variables['view'];
  $style_plugin = $view->style_plugin;

  $items = array();
  foreach ($variables['rows'] as $i => $text) {
    $index = $i + 1;
    $items[$index] = array(
      'title' => '',
      'text' => $text,
    );
    if (isset($variables['options']['title'])) {
      $items[$index]['title'] = $style_plugin->get_field($i, $variables['options']['title']);
    }
  }

  // Detect style base.
  global $is_https;
  if ($profile = acton_profile_get_active_profile()) {
    $variables['style_base'] = $style_base = ($is_https ? 'https' : 'http') . '://' . $profile->getServerEndpointBase();
  }

  // Render gallery.

  return theme('anu_feature_ticker', array(
    'items' => $items,
    'style_base' => $style_base,
    'link_listing' => $variables['options']['link_listing'],
    'live_preview' => !empty($view->override_path),
  ));
}

/**
 * Implements template_preprocess_HOOK() for anu_feature_ticker.
 */
function template_preprocess_anu_feature_ticker(&$variables) {
  $path = drupal_get_path('module', 'anu_feature');
  drupal_add_js($path . '/anu_feature.js');
  drupal_add_js($path . '/jquery.newsTicker.js');
}

/**
 * Renders a view feature gallery.
 */
function theme_views_view_anu_feature_gallery($variables) {
  $view = $variables['view'];
  $style_plugin = $view->style_plugin;

  // Prepare items.
  $option_keys = array('title', 'image', 'image_small', 'image_thumbnail', 'url');
  $defaults = array_fill_keys($option_keys, NULL);
  $items = array();
  foreach ($variables['rows'] as $i => $text) {
    $index = $i + 1;
    $items[$index] = $defaults;
    foreach ($option_keys as $field) {
      if (isset($variables['options'][$field])) {
        $items[$index][$field] = $style_plugin->get_field($i, $variables['options'][$field]);
      }
    }
    $items[$index]['text'] = $text;
    // Stop at four features.
    if ($index == 4) {
      break;
    }
  }

  // Detect style base.
  global $is_https;
  if ($profile = acton_profile_get_active_profile()) {
    $variables['style_base'] = $style_base = ($is_https ? 'https' : 'http') . '://' . $profile->getServerEndpointBase();
  }

  // Render gallery.
  return theme('anu_feature_gallery', array(
    'items' => $items,
    'style_base' => $style_base,
    'live_preview' => !empty($view->override_path),
  ));
}

/**
 * Implements template_preprocess_HOOK() for views_view_anu_feature_gallery.
 */
function template_preprocess_anu_feature_gallery(&$variables) {
  drupal_add_js(drupal_get_path('module', 'anu_feature') . '/anu_feature.js');
}


function template_preprocess_anu_fotorama_view(&$variables) {
  global $is_https;
  if ($profile = acton_profile_get_active_profile()) {
    $variables['style_base'] = $style_base = ($is_https ? 'https' : 'http') . '://' . $profile->getServerEndpointBase();
  }

  $path = drupal_get_path('module', 'anu_feature');
  drupal_add_css($path . '/anu_feature.css');
  drupal_add_css("//style.anu.edu.au/_anu/4/style/fotorama.css", 'external');
  drupal_add_js('//style.anu.edu.au/_anu/4/min/fotorama.min.js', array(
    'type' => 'external',
    'group' => JS_LIBRARY,
  ));

  // Add settings.
  $variables['attributes_array']['class'][] = 'anu-fotorama';
  $options = $variables['options'];
  $variables['attributes_array']['data-width'] = '100%';
  $variables['attributes_array']['data-thumbmargin'] = '5';
  if (!empty($options['ratio'])) {
    $variables['attributes_array']['data-ratio'] = $options['ratio'];
  }

  if (!empty($options['glimpse'])) {
    $variables['attributes_array']['data-glimpse'] = $options['glimpse'];
  }

  if (!empty($options['startindex'])) {
    $variables['attributes_array']['data-startindex'] = $options['startindex'];
  }

  if (!empty($options['margin'])) {
    $variables['attributes_array']['data-margin'] = $options['margin'];
  }

  $variables['attributes_array']['data-transition'] = empty($options['transition']) ? 'slide' : $options['transition'];
  $variables['attributes_array']['data-arrows'] = empty($options['arrows']) ? 'false' : ($options['arrows'] == 'always' ? 'always' : 'true');
  $variables['attributes_array']['data-nav'] = empty($options['nav']) ? 'false' : $options['nav'];
  $variables['attributes_array']['data-click'] = empty($options['click']) ? 'false' : 'true';
  $variables['attributes_array']['data-swipe'] = empty($options['swipe']) ? 'false' : 'true';
  $variables['attributes_array']['data-trackpad'] = empty($options['trackpad']) ? 'false' : 'true';
  $variables['attributes_array']['data-allowfullscreen'] = empty($options['allowfullscreen']) ? 'false' : 'true';
}

function template_process_anu_fotorama_view(&$variables) {
  $variables['attributes'] = drupal_attributes($variables['attributes_array']);
}

function theme_anu_fotorama_view($variables) {
  $output = '<div' . $variables['attributes'] . '>';
  $output .= implode('', $variables['rows']);
  $output .= '</div>';
  return $output;
}
