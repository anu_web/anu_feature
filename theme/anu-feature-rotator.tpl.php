<?php
/**
 * @file
 * Featured item template.
 *
 * This template lays out the featured into a box to be insert as a "slide" in
 * the rotator "slideshow".
 *
 * Available variables:
 * - $items: Raw feature items array.
 * - $attributes: Attributes string for feature rotator.
 * - $style_base: Base of the absolute URL containing "ANU window" image
 *     locations. This variable does not containing a trailing slash.
 * - $features: Rendered feature items array. Each value is now a feature-box.
 *     This array is non-empty iff (sic) there is at least one value in $items
 *     and $profile is not NULL.
 * - $fade: Whether to add a gradient layer for ticker.
 * - $nav_type: Type of navigation, either 'full' or 'minimal'.
 * - $nav_attributes: Attributes string for the rotator navigation.
 * - $nav_link_attributes: An array of attribute strings for navigation links,
 *     keyed by feature index.
 * - $live_preview: Whether the rotator is being previewed.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php foreach ($features as $feature): ?>
    <?php print $feature; ?>
  <?php endforeach; ?>
  <?php if ($fade): ?>
    <div class="features-fade"></div>
  <?php endif; ?>
  <div<?php print $nav_attributes; ?>>
    <ul class="anu-feature-nav-<?php print drupal_html_class($nav_type); ?>">
      <?php if ($nav_type == 'minimal'): ?>
        <li><a href="#" class="anu-feature-nav-link anu-feature-nav-link-prev"><img src="<?php print $style_base; ?>/images/icons/laquobutton.png" alt="Previous" title="Previous" /></a></li>
      <?php endif; ?>
      <?php if ($nav_type == 'full'): ?>
        <?php foreach ($nav_link_attributes as $counter => $link_attributes): ?>
          <li><a<?php print $link_attributes; ?>><?php print $counter; ?></a></li>
        <?php endforeach; ?>
      <?php endif; ?>
      <li><a href="#" class="anu-feature-nav-link anu-feature-nav-link-playpause"><img src="<?php print $style_base; ?>/images/icons/pause2.png" id="anu-feature-playpause" alt="Pause" title="Pause" /></a></li>
      <?php if ($nav_type == 'minimal'): ?>
        <li><a href="#" class="anu-feature-nav-link anu-feature-nav-link-next"><img src="<?php print $style_base; ?>/images/icons/raquobutton.png" alt="Next" title="Next" /></a></li>
      <?php endif; ?>
    </ul>
  </div>
</div>