<?php
/**
 * @file
 * Featured item template.
 *
 * This template lays out the featured into a box to be insert as a "slide" in
 * the rotator "slideshow".
 *
 * Available variables:
 * - $site: Site profile information array, containing:
 *     - 'profile': active Acton profile.
 *     - 'return_ssl': boolean flag indicating whether the active profile
 *         requires SSL resource links.
 *     - 'style_base': Base of the absolute URL containing "ANU window" image
 *         locations. This variable does not containing a trailing slash.
 * - $style_version: Style version.
 * - $counter: Counter of the current item in the list of items, starting at 1.
 * - $title: Featured item HTML-escaped title without block.
 * - $title_clean: Featured item HTML-escaped title stripped of tags, suitable
 *     for attribute values.
 * - $image: A 320px by 215px feature image block.
 * - $text: Featured item text to place at the of the feature-text block.
 * - $url: Optional title for the "read more" link.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($image && $image_position == 'before'): ?>
    <div class="feature-image">
      <?php print $image; ?>
    </div>
  <?php endif; ?>
  <?php if ($window): ?>
    <div class="feat-letters">
      <img class="feat-letters-png" src="<?php print $site['style_base']; ?>/<?php print $style_version; ?>/images/features/anu_window_text_white.png" width="320" height="215" title="<?php print $title_clean; ?>" alt="<?php print $title_clean; ?>" />
    </div>
  <?php endif; ?>
  <div class="feature-text" id="feature-text-<?php print $counter; ?>">
    <h2><?php print $title; ?></h2>
    <?php print $text; ?>
    <?php if ($url): ?>
    <p class="feat-more">&raquo;&nbsp;<a href="<?php print check_url($url); ?>">read more</a></p>
    <?php endif; ?>
  </div>
  <?php if ($image && $image_position == 'after'): ?>
  <div class="feature-image">
    <?php print $image; ?>
  </div>
  <?php endif; ?>
</div>