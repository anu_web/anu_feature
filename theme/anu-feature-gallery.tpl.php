<?php
/**
 * @file
 * Full-width large feature rotator.
 *
 * This template lays out four featured items in a full-width rotator with a
 * large image.
 *
 * Available variables:
 * - $items: Feature items array.
 * - $live_preview: Whether the rotator is being previewed.
 */
?>
<div id="f4">
  <div id="f4-features">
    <?php foreach ($items as $index => $item): ?>
      <div id="f4-feature-<?php print $index; ?>">
        <div class="f4-image">
          <div class="hide-rsp"><?php print $item['image']; ?></div>
          <div class="show-rsp"><?php print $item['image_small']; ?></div>
        </div>
        <div class="f4-image-text">
          <a href="#" class="right padtop control">
            <img src="<?php print $style_base; ?>/images/icons/play2.png" alt="Play" class="control-play">
            <img src="<?php print $style_base; ?>/images/icons/pause2.png" alt="Pause" class="control-pause">
          </a>
          <div class="f4-title"><a href="<?php print $item['url']; ?>"><?php print $item['title']; ?></a></div>
          <div class="f4-brief">
            <?php print $item['text']; ?>
            <span class="readmore"><a href="<?php print $item['url']; ?>">read&nbsp;more</a>&nbsp;»</span>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div id="f4-shorts">
    <?php foreach ($items as $index => $item): ?>
      <div id="f4-short-<?php print $index; ?>">
        <div class="f4-short-image"><?php print $item['image_thumbnail']; ?></div>
        <div class="f4-short-title"><a href="<?php print $item['url']; ?>"><?php print $item['title']; ?></a></div>
        <div class="f4-short-brief"><?php print $item['text']; ?></div>
      </div>
    <?php endforeach; ?>
  </div>
</div>