<div class="full bg-uni25">
  <div class="box">
    <h2>Notices</h2>
    <ul class="newsticker nobullet noindent nopad">
      <?php foreach ($variables['items'] as $item) { ?>
        <li>
          <span class="large"><?php echo $item['title']; ?></span> &mdash;
          <?php echo $item['text']; ?>
        </li>
      <?php } ?>
    </ul>
    <?php if (!empty($variables['link_listing'])) { ?>
      <p class="right nopad padtop readmore">
        <?php echo l('read all notices &raquo;', $variables['link_listing'], array('html' => TRUE)); ?>
      </p>
    <?php } ?>
    <p class="left nopad padtop">
      <img src="//style.anu.edu.au/_anu/images/icons/laquobutton.png"
           alt="Previous" title="Previous" id="prev-btn"/>
      <img src="//style.anu.edu.au/_anu/images/icons/pause2.png" alt="Pause"
           title="Pause" id="pause-btn"/>
      <img src="//style.anu.edu.au/_anu/images/icons/play2.png" alt="Play"
           title="Play" id="play-btn"/>
      <img src="//style.anu.edu.au/_anu/images/icons/raquobutton.png" alt="Next"
           title="Next" id="next-btn"/>
    </p>
  </div>
</div>
